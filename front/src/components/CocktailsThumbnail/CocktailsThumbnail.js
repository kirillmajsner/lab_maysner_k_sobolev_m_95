import React from 'react';
import {apiURL} from "../../constants";

const styles = {
    width: '250px',
    height: '275px',
    float: 'left'
};

const CocktailsThumbnail = props => {
    let image = null;

    if (props.image) {
        image = apiURL + '/uploads/' + props.image;
    }

    if (props.image === 'null') {
        return null
    }

    return <img src={image} style={styles} className="img-thumbnail" alt="cocktail"/>;
};

export default CocktailsThumbnail;

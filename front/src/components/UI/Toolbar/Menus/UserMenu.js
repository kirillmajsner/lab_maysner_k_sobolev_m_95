import React from 'react';
import {NavLink as RouterNavLink} from 'react-router-dom';
import {DropdownItem, DropdownMenu, DropdownToggle, NavLink, UncontrolledDropdown} from "reactstrap";

const UserMenu = ({user, logout}) => (
    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            Hello, {user.displayName}!
            <img style={{margin: '5px'}} src={user.avatarImage} alt="avatar"/>
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem divider/>
            <DropdownItem onClick={logout}>
                Logout
            </DropdownItem>
            <DropdownItem>
                <NavLink tag={RouterNavLink} to="/myCocktails">My cocktails</NavLink>
            </DropdownItem>
            <DropdownItem>
                <NavLink tag={RouterNavLink} to="/addCocktail">Add cocktails</NavLink>
            </DropdownItem>
            <DropdownItem divider/>
        </DropdownMenu>
    </UncontrolledDropdown>

);

export default UserMenu
import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import {createCocktail} from "../../store/actions/cocktailActions";


class AddNewCocktail extends Component {

    state = {
        name: '',
        ingredients: [{ingredient: '', amount: ''}],
        recipe: '',
        image: null
    };
    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    ingredientChangeHandler = (event, idx) => {
        const ingredient = {...this.state.ingredients[idx]};

        ingredient[event.target.name] = event.target.value;

        const ingredients = [...this.state.ingredients];
        ingredients[idx] = ingredient;

        this.setState({ingredients});
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };


    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        const ingredients = JSON.stringify(this.state.ingredients);

        formData.append('name', this.state.name);
        formData.append('image', this.state.image);
        formData.append('recipe', this.state.recipe);
        formData.append('ingredients', ingredients);

        this.props.createCocktail(formData)
    };


    addIngredient = event => {
        event.preventDefault();
        this.setState({
            ingredients: [...this.state.ingredients, {ingredient: '', amount: ''}]
        })
    };


    removeIngredient = index => {
        const ingredients = [...this.state.ingredients];

        ingredients.splice(index, 1);

        this.setState({ingredients});

    };


    createCocktail = cocktailsData => {
        this.props.createCocktail(cocktailsData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="name"
                        type="text"
                        title="Name"
                        value={this.state.name}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter cocktail name "
                    />


                    <FormGroup>
                        <Label>Ingredients:</Label>
                        {this.state.ingredients.map((ind, idx) => (
                            <div style={{marginLeft: '17%'}} key={idx}>
                                Name: <input type="text" name='ingredient'
                                             onChange={(event) => this.ingredientChangeHandler(event, idx)}/>
                                Amount: <input type="text" name='amount'
                                               onChange={(event) => this.ingredientChangeHandler(event, idx)}/>

                                {idx > 0 && <Button color='danger' style={{marginLeft: '5px'}} type="button"
                                                    onClick={() => this.removeIngredient(idx)}>X</Button>}
                            </div>
                        ))}
                        <Button onClick={this.addIngredient}>Add Ingredient</Button>
                    </FormGroup>

                    <FormElement
                        propertyName="recipe"
                        type="textarea"
                        title="Recipe"
                        value={this.state.recipe}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter recipe "
                    />

                    <FormGroup row>
                        <Label sm={2} for="image">Image</Label>
                        <Col sm={10}>
                            <Input
                                type="file"
                                name="image" id="image"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Create Cocktail
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}


const mapDispatchToProps = dispatch => ({
    createCocktail: cocktailData => dispatch(createCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(AddNewCocktail);

import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Card, CardBody, CardFooter, CardTitle, ListGroup, ListGroupItem} from "reactstrap";
import CocktailThumbnail from "../../components/CocktailsThumbnail/CocktailsThumbnail";
import {fetchCocktail} from "../../store/actions/cocktailActions";


class SingleCocktail extends Component {


    componentDidMount() {
        this.props.fetchCocktail(this.props.match.params.id);
    };

    render() {
        let ingredients = this.props.cocktail && this.props.cocktail.ingredients.map(ingredient => {
            return (
                <ListGroup style={{width: '400px', marginBottom: '10px', marginLeft: '30%'}} key={ingredient._id}>
                    <ListGroupItem><span>{ingredient.ingredient}</span> - <span>{ingredient.amount}</span></ListGroupItem>
                </ListGroup>
            )
        });
        return (
            <Fragment>
                {this.props.cocktail ? <Card>
                        <CardBody>
                            <CardTitle style={{marginLeft: '30%'}}><h1>{this.props.cocktail.name}</h1></CardTitle>
                            <CocktailThumbnail image={this.props.cocktail.image}/>
                            <h3 style={{marginLeft: '30%'}}>Ingredients: </h3>
                            {ingredients}
                        </CardBody>
                        <CardFooter><h3>Recipe: </h3>{this.props.cocktail.recipe}</CardFooter>
                    </Card>

                    : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cocktail: state.cocktails.cocktail,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchCocktail: id => dispatch(fetchCocktail(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SingleCocktail);

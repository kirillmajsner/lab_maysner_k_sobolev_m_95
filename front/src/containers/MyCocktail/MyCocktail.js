import React, {Fragment, Component} from 'react';
import {connect} from "react-redux"
import {fetchCocktails} from "../../store/actions/cocktailActions";
import CocktailsThumbnail from "../../components/CocktailsThumbnail/CocktailsThumbnail";
import {Alert, Card, CardBody, CardTitle, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";


class MyCocktail extends Component {


    componentDidMount() {
        this.props.fetchCocktail();
    }


    render() {
        return (
            <Fragment>
                <h2>My cocktails: </h2>
                {this.props.cocktails && this.props.cocktails.map(cocktail => {
                    return (
                        <Card style={{margin: '20px'}} key={cocktail._id}>
                            <CardTitle>
                                {cocktail.published === true ?
                                    <Alert color="success">
                                        Your cocktail is published
                                    </Alert> :
                                    <Alert color="danger">
                                        Your cocktail is being moderated
                                    </Alert>
                                }
                            </CardTitle>
                            <CocktailsThumbnail image={cocktail.image}/>
                            <CardBody>
                                <NavLink tag={RouterNavLink}
                                         to={'/cocktail/' + cocktail._id}>{cocktail.name}</NavLink>
                            </CardBody>
                        </Card>
                    )
                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails.cocktails,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchCocktail: () => dispatch(fetchCocktails())
});

export default connect(mapStateToProps, mapDispatchToProps)(MyCocktail);
import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {NavLink as RouterNavLink} from "react-router-dom";


import {cocktailPublish, deleteCocktail, fetchCocktails} from "../../store/actions/cocktailActions";
import {Alert, Button, Card, CardBody, CardFooter, CardTitle, NavLink} from "reactstrap";
import CocktailThumbnail from "../../components/CocktailsThumbnail/CocktailsThumbnail";


class Cocktail extends Component {
    componentDidMount() {
        this.props.getCocktails()
    }

    render() {
        return (
            <Fragment>
                <h4>Cocktails: </h4>
                {this.props.cocktails.map(cocktail => {
                    return (
                        <div key={cocktail._id} className='float-left'>
                            <Card body outline color="info"
                                  style={{
                                      border: '2px solid',
                                      marginTop: '30px',
                                      margin: '30px'
                                  }}>
                                <CardTitle>{cocktail.published === false ?
                                    <Alert color='danger'>Your cocktail is being moderated</Alert> :
                                    <Alert>Author: <strong>{cocktail.user.displayName}</strong></Alert>}
                                </CardTitle>
                                <CocktailThumbnail image={cocktail.image}/>
                                <CardBody>
                                    <NavLink tag={RouterNavLink}
                                             to={'/cocktail/' + cocktail._id}>{cocktail.name}</NavLink>
                                </CardBody>
                                {this.props.user && this.props.user.role === 'admin' ?
                                    <CardFooter>
                                        {cocktail.published === false ?
                                            <Button className='float-right'
                                                    onClick={() => this.props.cocktailPublish(cocktail._id)}
                                                    type="submit"
                                                    color="success"
                                                    style={{marginLeft: '10px'}}>Publish</Button> : null}
                                        <Button className='float-right' color="danger"
                                                onClick={() => this.props.deleteCocktail(cocktail._id)}>Delete</Button>
                                    </CardFooter> : null
                                }
                            </Card>
                        </div>
                    )
                })}

            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    cocktails: state.cocktails.cocktails
});

const mapDispatchToProps = dispatch => ({
    getCocktails: () => dispatch(fetchCocktails()),
    deleteCocktail: (id, cocktailId) => dispatch(deleteCocktail(id, cocktailId)),
    cocktailPublish: (id, cocktailId) => dispatch(cocktailPublish(id, cocktailId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktail);
import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";


import AddNewCocktail from "./components/AddNewCocktail/AddNewCocktail";
import AllCocktail from "./containers/AllCocktails/AllCocktails";
import SingleCocktail from "./containers/SingleCocktail/SingleCocktail";
import MyCocktail from "./containers/MyCocktail/MyCocktail";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props}/> : <Redirect to="/"/>;
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={AllCocktail}/>
            <Route path="/cocktail/:id" exact component={SingleCocktail}/>
            <ProtectedRoute
                isAllowed={user}
                path="/addCocktail"
                exact
                component={AddNewCocktail}
            />
            <ProtectedRoute
                isAllowed={user}
                path="/myCocktails"
                exact
                component={MyCocktail}
            />
        </Switch>
    );
};

export default Routes;
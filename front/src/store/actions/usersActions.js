import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";
import axios from '../../axios-coctail';


export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';


export const loginUserSuccess = (data) => ({type: LOGIN_USER_SUCCESS, data});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const logoutUser = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.delete('/user/sessions', config).then(
            () => {
                dispatch({type: LOGOUT_USER});
                NotificationManager.success('Logged out');
            },
            error => {
                NotificationManager.error('Could not logout!');
            }
        )
    }
};


export const facebookLogin = userData => {
    return dispatch => {
        return axios.post('/user/facebookLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                NotificationManager.success('Logged in via Facebook');
                dispatch(push('/'));
            },
            () => {
                dispatch(loginUserFailure('Login via Facebook failed'));
            }
        )
    }
};
import axios from '../../axios-coctail';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router'

export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';

const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAILS_SUCCESS, cocktails});
const fetchCocktailSuccess = cocktail => ({type: FETCH_COCKTAIL_SUCCESS, cocktail});


export const fetchCocktails = () => {
    return dispatch => {
        return axios.get('/cocktail').then(
            response => dispatch(fetchCocktailsSuccess(response.data))
        )
    }
};

export const fetchCocktail = cocktailId => {
    return dispatch => {
        return axios.get(`/cocktail/${cocktailId}`).then(
            response => dispatch(fetchCocktailSuccess(response.data))
        )
    }
};

export const createCocktail = cocktailId => {
    return dispatch => {
        return axios.post('/cocktail', cocktailId).then(
            () => {
                dispatch(fetchCocktails());
                NotificationManager.success('Cocktail created');
                dispatch(push('/'));
            },
            error => {
                NotificationManager.error('Error creating cocktail!');
            }
        )
    }
};

export const cocktailPublish = (id, cocktailId) => {
    return (dispatch) => {
        return axios.post(`/cocktail/${id}/toggle_published`).then(() => {
            NotificationManager.success('Cocktail has been published');
            dispatch(fetchCocktails(cocktailId));
        });
    }
};


export const deleteCocktail = (id, cocktailId) => {
    return dispatch => {
        return axios.delete(`/cocktail/${id}`).then(
            () => {
                NotificationManager.error('Cocktail deleted!');
                dispatch(fetchCocktails(cocktailId));
                dispatch(push('/'));
            }
        )
    };
};
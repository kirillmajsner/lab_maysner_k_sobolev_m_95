import {FETCH_COCKTAIL_SUCCESS, FETCH_COCKTAILS_SUCCESS} from "../actions/cocktailActions";

const initialState = {
    cocktails: [],
    cocktail: null,
};

const cocktailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAILS_SUCCESS:
            return {
                ...state,
                cocktails: action.cocktails
            };
        case FETCH_COCKTAIL_SUCCESS:
            return {
                ...state,
                cocktail: action.cocktail
            };
        default:
            return state;
    }
};

export default cocktailsReducer;
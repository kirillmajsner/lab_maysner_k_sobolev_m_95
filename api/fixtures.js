const nanoid = require("nanoid");
const mongoose = require('mongoose');

const Cocktail = require('./models/Cocktail');
const User = require('./models/User');
const config = require('./config');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            role: 'admin',
            username: 'admin',
            token: nanoid(),
            password: nanoid(),
            displayName: 'Admin',
            facebookId: '100036992001298',
            avatar: null
        },
    );

    await Cocktail.create(
        {
            user: user,
            name: 'Long Island',
            image: 'long.jpeg',
            recipe: 'Fill a cocktail shaker with ice. Pour vodka, rum, gin, tequila, triple sec, and sour mix over' +
            ' ice; cover and shake. Pour cocktail into a Collins or hurricane glass; top with splash of cola for' +
            ' color. Garnish with a lemon slice.',
            published: true,
            ingredients: [
                {ingredient: 'vodka', amount: '15ml'},
                {ingredient: 'Silver tequila', amount: '15ml'},
                {ingredient: 'Gin', amount: '15ml'},
                {ingredient: 'White Rum', amount: '15ml'},
                {ingredient: 'Fresh Lemon Juice', amount: '15ml'}
            ],
        },
        {
            user: user,
            name: 'White Russian',
            image: 'White.jpg',
            recipe: 'Fill a cocktail shaker with ice. Pour vodka, rum, gin, tequila, triple sec, and sour mix over' +
            ' ice; cover and shake. Pour cocktail into a Collins or hurricane glass; top with splash of cola for' +
            ' color. Garnish with a lemon slice.',
            published: true,
            ingredients: [
                {ingredient: 'vodka', amount: '15ml'},
                {ingredient: 'Silver tequila', amount: '15ml'},
                {ingredient: 'Gin', amount: '15ml'},
                {ingredient: 'White Rum', amount: '15ml'},
                {ingredient: 'Fresh Lemon Juice', amount: '15ml'}
            ],
        },
        {
            user: user,
            name: 'GIN GIN MULE',
            image: 'Gin.jpg',
            recipe: 'Fill a cocktail shaker with ice. Pour vodka, rum, gin, tequila, triple sec, and sour mix over' +
            ' ice; cover and shake. Pour cocktail into a Collins or hurricane glass; top with splash of cola for' +
            ' color. Garnish with a lemon slice.',
            published: true,
            ingredients: [
                {ingredient: 'vodka', amount: '15ml'},
                {ingredient: 'Silver tequila', amount: '15ml'},
                {ingredient: 'Gin', amount: '15ml'},
                {ingredient: 'White Rum', amount: '15ml'},
                {ingredient: 'Fresh Lemon Juice', amount: '15ml'}
            ],
        },
        {
            user: user,
            name: 'Painkiller',
            image: 'Killer.jpg',
            recipe: 'Fill a cocktail shaker with ice. Pour vodka, rum, gin, tequila, triple sec, and sour mix over' +
            ' ice; cover and shake. Pour cocktail into a Collins or hurricane glass; top with splash of cola for' +
            ' color. Garnish with a lemon slice.',
            published: true,
            ingredients: [
                {ingredient: 'vodka', amount: '15ml'},
                {ingredient: 'Silver tequila', amount: '15ml'},
                {ingredient: 'Gin', amount: '15ml'},
                {ingredient: 'White Rum', amount: '15ml'},
                {ingredient: 'Fresh Lemon Juice', amount: '15ml'}
            ],
        },
    );


    return connection.close();
};


run().catch(error => {
    console.log('Something wrong happened ...', error);
});
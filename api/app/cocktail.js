const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');


const config = require("../config");
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');
const Cocktail = require('../models/Cocktail');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
    try {
        let criteria = {published: true};

        if (req.user && req.user.role === 'user') {
            criteria = {
                $or: [
                    {published: true},
                    {user: req.user._id}
                ]
            }
        }
        if (req.user && req.user.role === 'admin') {
            criteria = {}
        }

        const cocktail = await Cocktail.find(criteria).populate('user', '_id, displayName');


        res.send(cocktail);
    } catch (e) {
        if (e.name === 'ValidationError') {
            return res.status(400).send(e)
        }
        return res.status(500).send(e)
    }
});

router.get('/:id', (req, res) => {
    Cocktail.findById(req.params.id)
        .then(cocktail => {
            if (cocktail) res.send(cocktail);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});


router.post('/', [auth, upload.single('image')], (req, res) => {
    const cocktailInfo = {
        user: req.user._id,
        name: req.body.name,
        recipe: req.body.recipe,
        ingredients: JSON.parse(req.body.ingredients)
    };

    if (req.file) {
        cocktailInfo.image = req.file.filename;
    }

    const cocktail = new Cocktail(cocktailInfo);

    cocktail.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.post('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {
    const cocktail = await Cocktail.findById(req.params.id);

    if (!cocktail) {
        return res.sendStatus(404);
    }

    cocktail.published = !cocktail.published;

    await cocktail.save();

    return res.send(cocktail);
});

router.delete('/:id', [auth, permit("admin")], (req, res) => {
    Cocktail.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});

module.exports = router;
